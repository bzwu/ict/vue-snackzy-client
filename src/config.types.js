export const API_URL = 'http://api.snackzy.test';
export const API_ENDPOINT_CLIENT_CREATE = '/client/create';
export const API_ENDPOINT_QUESTION_GET_NEXT = '/question/get-next';
export const API_ENDPOINT_QUESTION_ANSWER = '/question/answer';
export const API_ENDPOINT_QUESTION_WRONG = '/question/get-wrong';

export const DEFAULT_COOKIE_LIFETIME = '7d';
export const CLIENT_ID_COOKIE_LIFETIME = '1y';
export const COOKIE_NAME_CLIENT_ID = 'clid';

export const DEFAULT_QUESTION_STATE = {
    id: 0,
    question: '',
    answers: [],
    nextQuestionId: 0
};
