import Vue from 'vue';
import Vuex from 'vuex';
import axios from 'axios';
import {API_ENDPOINT_CLIENT_CREATE, API_ENDPOINT_QUESTION_ANSWER, API_ENDPOINT_QUESTION_GET_NEXT, API_ENDPOINT_QUESTION_WRONG, API_URL, CLIENT_ID_COOKIE_LIFETIME, COOKIE_NAME_CLIENT_ID, DEFAULT_QUESTION_STATE} from '../config.types';

Vue.use(Vuex);


export default new Vuex.Store({
    state: { // Data holding
        isInitialized: false,
        client: null,
        question: DEFAULT_QUESTION_STATE,
        isProcessing: false,
        wrongQuestions: []
    },
    getters: { // Getters
        isInitialized: state => state.isInitialized,
        client: state => state.client,
        question: state => state.question,
        isProcessing: state => state.isProcessing,
        wrongQuestions: state => state.wrongQuestions
        //questionById: (state, id) => state.question.filter(question => question.id === id)
    },
    mutations: { // "Setters"
        setClient(state, uuid) {
            state.client = uuid;
        },
        setQuestion(state, question) {
            state.question = question
        },
        setIsInitialized(state, initialized) {
            state.isInitialized = initialized;
        },
        setIsProcessing(state, processing) {
            state.isProcessing = processing;
        },
        addWrongQuestion(state, question) {
            let hasQuestion = state.wrongQuestions.filter(wrongQuestion => wrongQuestion.id === question.id);

            if (!hasQuestion) {
                state.wrongQuestions.push(question);
            }
        }
    },
    actions: { // Binds State to the API
        fetchClient(context) {
            axios.get(API_URL + API_ENDPOINT_CLIENT_CREATE).then(response => {
                let data = response.data;

                if (data.success === true && undefined !== data.client) {
                    let uuid = data.client;

                    context.commit('setClient', uuid);
                    this.$cookies.set(COOKIE_NAME_CLIENT_ID, uuid, CLIENT_ID_COOKIE_LIFETIME);
                }
            }).catch(e => {
                //console.log(e);
                context.commit('setIsInitialized', false);
            });
        },
        fetchNextQuestion(context) {
            axios.get(`${API_URL}${API_ENDPOINT_QUESTION_GET_NEXT}/${context.getters.client}`).then(response => {
                let data = response.data;

                context.commit('setQuestion', data.question);
                context.commit('setIsInitialized', true);
            }).catch(e => {
                //console.log(e);
                context.commit('setIsInitialized', false);
            }).then(() => {
                context.commit('setIsProcessing', false);
            });
        },
        commitAnswer(context, answer) {
            let form = new FormData();

            form.append('uuid', context.getters.client);
            console.log(context.getters.client);
            context.commit('setIsProcessing', true);

            axios({
                method: 'POST',
                url: `${API_URL}/question/${context.getters.question.id}/answer/${answer}`,
                data: form,
                header: {
                    /*'Content-Type': 'application/json',
                    'Access-Control-Expose-Headers': '*',
                    'Access-Control-Allow-Origin': '*',
                    'Access-Control-Allow-Methods': '*',
                    'Access-Control-Allow-Headers': '*'*/
                }
            }).then(response => {
                let data = response.data;

                if (data.success === true) {
                    // answered correct
                } else {
                    // answered false
                }
            }).catch(e => {
                console.error(e);
            });
        },
        fetchWrongQuestions(context) {
            axios.get(`${API_URL}${API_ENDPOINT_QUESTION_WRONG}/${context.getters.client}`).then(response => {
                let data = response.data;

                data.wrongQuestions.forEach(question => {
                    context.commit('addWrongQuestion', question);
                });
            }).catch(e => {
                console.log(e);
            }).then(() => {
                context.dispatch('fetchNextQuestion');
            });
        }
    }
});

