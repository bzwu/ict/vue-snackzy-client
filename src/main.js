import Vue from 'vue';
import VueCookies from 'vue-cookies';
import App from './App.vue';
import router from './router';
import store from './store';
import axios from 'axios';
import '@babel/polyfill';
import './plugins/vuetify';
import './config.types';
import {
    COOKIE_NAME_CLIENT_ID,
    DEFAULT_COOKIE_LIFETIME
} from './config.types';

Vue.config.productionTip = false;
Vue.prototype.$http = axios;

VueCookies.config(DEFAULT_COOKIE_LIFETIME);
Vue.use(VueCookies);

new Vue({
    beforeCreate: function () {
        let clientUuid = this.$cookies.get(COOKIE_NAME_CLIENT_ID);

        if (clientUuid === null) {
            this.$store.dispatch('fetchClient');
        } else {
            this.$store.commit('setClient', clientUuid);
        }

        this.$store.dispatch('fetchNextQuestion');
    },
    router,
    store,
    render: h => h(App)
}).$mount('#app');
